from traverse import walk, become, remove_child_if
from c_ast import is_kind, is_function_decl, is_annotate_attr_ex, is_decl_stmt, \
      is_func_call_expr, lt_operator, assert_stmt, distinct_expr, array_subscript
from copy import deepcopy
__all__ = "rewrite_cuda",

def gen_type(name, include_self=True):
    result = set(name + str(x) for x in range(1, 5))
    if include_self:
        result.add(name)
    return result

def gen_types(*names):
    types = map(gen_type, names)
    result = set({})
    for ty in types:
        result = result.union(ty)
    return result

CUDA_TYPES = {"dim3", "dim4", "ushort"}\
    .union(gen_types("uint", "int", "float", "char", "uchar", "short", "ushort", "long", "ulong", "longlong", "ulonglong", "double"))

FILTER_CUDA = {
    "TypedefDecl": CUDA_TYPES,
    "CXXRecordDecl": CUDA_TYPES,
    "VarDecl": {"threadIdx", "blockIdx", "blockDim", "gridDim", "warpSize"},
    "FunctionDecl": {"__syncthreads","atomicAdd", "__requires"}
    .union("make_" + x for x in CUDA_TYPES)
}

LOCALS = {"threadIdx", "blockIdx"}
GLOBALS = {"gridDim", "blockDim"}
DIM = {
    "threadIdx": "blockDim",
    "blockIdx": "gridDim"
}


KNOWN_MEMBERS = LOCALS.union(GLOBALS)

def is_global(x): return is_annotate_attr_ex(x, "global")
def is_shared(x): return is_annotate_attr_ex(x, "shared")

def make_param(name):
    return dict(kind="ParmVarDecl", name=name, type=dict(qualType="const unsigned int"), isUsed=True)

def make_var(name):
    return dict(kind="VarDecl", name=name, type=dict(qualType="const unsigned int"))

def make_decl(name):
    return dict(kind="DeclStmt", inner=[make_var(name)])

def rewrite_cuda(obj):
    for func in obj["inner"]:
        if "FunctionDecl" != func.get("kind", None):
            continue

        # removed all __global__ attributes
        removed = remove_child_if(is_global, func["attrs"])
        # If there is at least one global attributes, then mark this function
        # as a kernel
        func["is_kernel"] = len(removed) > 0


        body = func["body"]
        cuda_locals = set()
        shared_locations = set()
        block_dims = {"x", }
        for child in walk(body):
            if is_decl_stmt(child):
                elems = child.get("inner", ())
                if len(elems) != 1 or "inner" not in elems[0]:
                    continue
                decl, = elems
                removed = remove_child_if(is_shared, decl["inner"])
                if len(decl["inner"]) == 0:
                    del decl["inner"]
                is_local = len(removed) > 0
                if is_local:
                    shared_locations.add(decl["name"])
                    param = make_param(decl["name"])
                    param["type"] = deepcopy(decl["type"])
                    if "qualType" in param["type"]:
                        param["type"]["qualType"] += " *"
                    func["params"].append(param)
                    # Remove node
                    child.clear()
                    child["kind"] = "CompoundStmt"
                    child["inner"] = []

            elif is_func_call_expr(child, "__syncthreads"):
                become(child, {
                    "kind": "SyncStmt",
                })

            elif is_kind(child, "MemberExpr"):
                base = child["base"]
                if not is_kind(base, "VarDecl") or base["name"] not in KNOWN_MEMBERS:
                    continue

                base["type"] = child["type"]
                base["name"] += "." + child["name"]
                if base["name"] == "blockIdx":
                    block_dims.add(child["name"])
                become(child, base)
                cuda_locals.add(base["name"])

        block_dims = list(sorted(block_dims))
        to_change = list(child for child in walk(body) if is_kind(child, "VarDecl") and child["name"] in shared_locations)
        for child in to_change:
            #continue
            new_child = deepcopy(child)
            # Change it to a parameter
            new_child["kind"] = "ParmVarDecl"
            for dim in block_dims:
                new_child = array_subscript(new_child, make_var("blockIdx." + dim))
                new_child["synthesized"] = True
            become(child, new_child)

        # Add local constraints blockIdx.x < threadIdx.x
        ty_locals = [name for name in sorted(cuda_locals) if name.split(".")[0] in LOCALS]
        ty_locals.sort()
        if len(ty_locals) > 0:
            stmt = assert_stmt(distinct_expr(list(map(make_var, ty_locals))))
            func["body"]["inner"].insert(0, stmt)
        for name in ty_locals:
            base, field = name.split(".")
            # Local
            dim = DIM[base] + "." + field
            if dim in cuda_locals:
                func["body"]["inner"].insert(0, assert_stmt(lt_operator(make_var(name), make_param(dim))))
        # Add variable/parameter declarations
        for name in sorted(cuda_locals):
            base, field = name.split(".")
            if base in LOCALS:
                # Local
                func["body"]["inner"].insert(0, make_decl(name))
            else:
                # Global
                func["params"].insert(0, make_param(name))

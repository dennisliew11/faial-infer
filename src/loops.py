from enum import Enum
from traverse import *
from c_ast import *

__all__ = ["rewrite_loops"]

"""
Structured loop inference.
"""

def is_plus_k(obj):
    return is_kind(obj, "UnaryOperator") and obj.get("opcode", None) == "++" \
        or is_kind(obj, "CompoundAssignOperator") and obj.get("opcode", None) == "+="

def range_expr(init=integer_literal(0), upper_bound=None, step=integer_literal(1), opcode="+"):
    return {
        "kind": "RangeExpr",
        "init": init,
        "upper_bound": upper_bound,
        "step": step,
        "opcode": opcode,
    }

def for_each_stmt(var, range, body):
    return {
        "kind": "ForEachStmt",
        "var": var,
        "range": range,
        "body": body
    }

FORWARD_STEP = {"++", "+=", "<<=", "*="}
BACKWARD_STEP = {"--", "-=", ">>=", "/="}

FORWARD_COND = {"<=", "<"}
BACKWARD_COND = {">=", ">"}

class Dir(Enum):
    FORWARD = 0
    BACKWARD = 1

def get_direction(obj):
    step = obj.get("inc", None)
    if step is not None and step.get("opcode", None) in BACKWARD_STEP:
        return Dir.BACKWARD
    if step is not None and step.get("opcode", None) in FORWARD_STEP:
        return Dir.FORWARD

    cnd = obj.get("cond", None)
    if cnd is not None and cnd.get("opcode", None) in FORWARD_COND:
        return Dir.FORWARD
    if cnd is not None and cnd.get("opcode", None) in BACKWARD_COND:
        return Dir.BACKWARD

    raise ValueError("Could not infer the loop direction", obj)

def get_init(obj, loop_dir):
    init = obj.get("init", None)
    var = None
    step = obj.get("inc", None)
    cnd = obj.get("cond", None)
    if init is not None:
        if is_decl_stmt(init):
            # Ensure init has at least one child
            if len(init["inner"]) == 0:
                raise ValueError(init)
            var = init["inner"][0]
            # Get the initializer of the variable
            init = var["inner"][0]
            del var["inner"]
            del var["init"]
            return var, init
        elif is_kind(init, "BinaryOperator") and init.get('opcode', None) == '=':
            var = init["lhs"]
            init = init["rhs"]
            return var, init
        else:
            raise ValueError("Unsupported initialization pattern %r" % init)
    # Try to infer the variable name from the step
    if var is None and step is not None:
        var = step["lhs"]
    # Try to infer the variable name from the get
    if var is None and cnd is not None and cnd.get("lhs", None) is not None and cnd.get("rhs", None) is not None:
        elems = [cnd["lhs"], cnd["rhs"]]
        elems = list(filter(lambda x: x.get("kind", None) != "IntegerLiteral", elems))
        if len(elems) == 1:
            var = elems[0]
    if var is None:
        raise ValueError(obj)
    # There was no initial value;
    if loop_dir == Dir.FORWARD and init is None:
        init = integer_literal(0)
    elif loop_dir == Dir.BACKWARD and init is None:
        init = integer_literal(0xfffffffe)
    return var, init

def get_step(obj, var, loop_dir):
    step = obj.get("inc", None)
    # Step is unknown
    if step is None and loop_dir == Dir.FORWARD:
        step = plus_plus_var(var)
    if step is None and loop_dir == Dir.BACKWARD:
        step = minus_minus_var(var)

    if step.get("opcode", None) in {">>=", "<<="}:
        opcode = "pow2"
    else:
        opcode = "+"

    if is_kind(step, "UnaryOperator") and step.get("opcode", None) in {"++", "--"}:
        step = integer_literal(1)
    elif is_kind(step, "CompoundAssignOperator"):
        if step["opcode"] in {"*=", "/="} and is_kind(step["rhs"], "IntegerLiteral") and step["rhs"]["value"] > 0:
            opcode = "pow%d" % step["rhs"]["value"]
            step = integer_literal(1)
        else:
            step = step["rhs"]
    elif is_kind(step, "BinaryOperator"):
        if step["opcode"] in {">>="} and is_kind(step["rhs"], "IntegerLiteral") and step["rhs"]["value"] > 0:
            opcode = "pow2"
            step = step["rhs"]["value"]
    else:
        raise ValueError(repr(step))
    return step, opcode

def infer_range(obj):
    """
    The simplest loop example is `for (int i = 0; i < 100; i++)`:

    >>> loop = for_stmt(
    ... init=decl_stmt_var("i", init=integer_literal(0)),
    ... cond=lt_operator(
    ...    var_decl("i", type=int_type()),
    ...     integer_literal(100)),
    ... inc=plus_plus_var("i"),
    ... body=compound_stmt(),
    ... )
    >>> var, rng = infer_range(loop)

    The result is a variable and a loop.

    The inferred variable should be `i`:

    >>> var == var_decl("i", type=int_type())
    True

    The range should trivially inferred:

    >>> rng == range_expr(
    ... init = integer_literal(0),
    ... upper_bound = integer_literal(100),
    ... opcode = '+',
    ... step = integer_literal(1),
    ... )
    True

    """
    loop_dir = get_direction(obj)
    ub = obj["cond"]["rhs"]
    var, init = get_init(obj, loop_dir)

    # Switch upper-bound and init and increment if possible
    if loop_dir == Dir.BACKWARD:
        ub, init = init, ub
        ub = inc_operator(ub)
        if obj["cond"].get("opcode", None) == ">":
            init = inc_operator(init)
    if obj["cond"]["opcode"] == "<=":
        ub = inc_operator(ub)

    step, opcode = get_step(obj, var, loop_dir)

    rng = range_expr(
        init = init,
        upper_bound = ub,
        step = step,
        opcode = opcode,
    )
    return var, rng

def infer_foreach(obj):
    var, rng = infer_range(obj)
    return for_each_stmt(
        var = var,
        range = rng,
        body = obj["body"]
    )

def rewrite_loops(obj):
    for child in filter(is_for_stmt, walk(obj)):
        become(child, infer_foreach(child))


if __name__ == '__main__':
    import doctest
    doctest.testmod()

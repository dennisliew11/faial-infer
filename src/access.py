from traverse import *
from c_ast import *

__all__ = ["rewrite_access"]

"""
Memory access inference.
"""

def is_nd_access(obj):
    if is_kind(obj, 'ArraySubscriptExpr'):
        if is_var(obj['lhs']):
            return True
        return is_nd_access(obj['lhs'])
    return False

def get_accesses(obj):
    return find(obj, is_nd_access)

def access(loc, index, mode):
    return {
        "kind": "AccessStmt",
        "location": loc,
        "mode": mode,
        "index": index,
    }

RW_MODE = 'rw'
RO_MODE = 'ro'

def to_access(obj, mode):
    def aux(obj, indices):
        assert obj['kind'] == 'ArraySubscriptExpr'
        child = obj['lhs']
        indices.append(obj['rhs'])
        if is_var(child):
            return access(child["name"], indices, mode)
        else:
            return aux(child, indices)
    return aux(obj, [])

def to_rw_access(obj):
    return to_access(obj, RW_MODE)

def to_ro_access(obj):
    return to_access(obj, RO_MODE)

def rewrite_access(obj):
    def do_become(obj, elems):
        if len(elems) == 0:
            return
        if len(elems) == 1:
            stmt = elems[0]
        else:
            stmt = compound_stmt(elems)
        become(obj, stmt)

    for eq_obj in filter(is_assign_binary, walk(obj)):
        # READS
        accs = list(map(to_rw_access, get_accesses(eq_obj["lhs"])))
        # WRITES
        accs.extend(map(to_ro_access, get_accesses(eq_obj["rhs"])))
        do_become(eq_obj, accs)

    for inc_obj in filter(is_assign_unary, walk(obj)):
        accs = list(map(to_rw_access, get_accesses(inc_obj["subExpr"])))
        do_become(inc_obj, accs)

    for obj_update in filter((lambda x: is_method_call_expr(x) or is_call_expr(obj)), walk(obj)):
        accs = list(map(to_ro_access, get_accesses(obj_update["args"])))
        do_become(obj_update, accs)

    for child in filter(is_decl_stmt, walk(obj)):
        accs = list(map(to_ro_access, get_accesses(child["inner"][0])))
        do_become(child, accs)

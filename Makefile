all: run-examples run-pytest

test: run-examples run-pytest

run-examples:
	./scripts/test.sh

run-pytest:
	pytest

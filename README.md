# faial-infer: Extracts a concurrency signature from a CUDA program

`faial-infer` extracts a concurrency signature from a CUDA program. The tool
[`faial`](https://gitlab.com/cogumbreiro/faial) can then prove if a concurrency
signature is data-race free.

# Installation

## Setup

**Run this once to install dependencies.**
```bash
$ pip install -r requirements.txt
```

# Usage

`faial-infer` expects a serialized CUDA-program, which is obtained from
[`c-to-json`](https://gitlab.com/cogumbreiro/c-to-json).

```bash
$ ./faial-infer examples/2d-example.json
```

# Development

For testing we use [pytest](https://docs.pytest.org/en/latest/).

To run the tests of our project, just run:

```bash
$ pytest
```

Test modules are prefixed with `test_`.